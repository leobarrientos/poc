package com.latam.mos.poc.resources;

import java.io.IOException;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.latam.mos.poc.google.GoogleSheetsConnection;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@RestController
public class App 
{

	class Message {
		private String message;

		public Message(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	@RequestMapping(path = "/echo/{message}", method= RequestMethod.GET)
	public Message echo(@PathVariable("message") String message) {

		return new Message(message);
	}

	@RequestMapping(path = "/resources/fleet", method= RequestMethod.GET)
	public Message getFleet(){

		String message="";
		// Build a new authorized API client service.
		Sheets service = null;
		try {
			service = GoogleSheetsConnection.getSheetsService();


			// Prints the names and majors of students in a sample spreadsheet:
			// https://docs.google.com/spreadsheets/d/1v0J7CmJ3nfV6snxT5Or0xriF7LplyPodN/edit
			String spreadsheetId = "1v0J7CmJ3nfV6snxT5Or0xriF7LplyPodN-zLptr206c";
			String range = "A1:C8";
			ValueRange response;

			response = service.spreadsheets().values()
					.get(spreadsheetId, range)
					.execute();

			List<List<Object>> values = response.getValues();
			if (values == null || values.size() == 0) {
				System.out.println("No data found.");
			} else {
				for (List<Object> row : values) {
					System.out.printf("%s, %s\n", row.get(0), row.get(1));
					message=message+ row.get(0) +','+row.get(1)+'|';
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Message(message);
	}

	public static void main( String[] args )
	{
		System.out.println( "Hello World!" );
		SpringApplication.run(App.class, args);
	}
}
