package com.latam.mos.poc.google;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTestIT
{
	@Test
	public void firstEchoTest() {
		get("/echo/hello").then().assertThat().body("message", equalTo("hello"));
	}
}
