FROM java:8
VOLUME /tmp
ADD target/resources-0.0.1-SNAPSHOT.jar app.jar
ADD src/main/java/com/latam/mos/poc/google/client_secret.json /com/latam/mos/poc/google/client_secret.json
ADD src/main/java/com/latam/mos/poc/google/sheets.googleapis.com-java-quickstart/StoredCredential /com/latam/mos/poc/google/sheets.googleapis.com-java-quickstart/StoredCredential
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
